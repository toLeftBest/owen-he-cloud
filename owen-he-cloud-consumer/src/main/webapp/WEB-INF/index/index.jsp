<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	String ctx = request.getContextPath();
%>
<!DOCTYPE HTML>
<style type="text/css">
	table.gridtable {
		font-family: verdana,arial,sans-serif;
		font-size:11px;
		color:#333333;
		border-width: 1px;
		border-color: #666666;
		border-collapse: collapse;
	}
	table.gridtable th {
		border-width: 1px;
		padding: 8px;
		border-style: solid;
		border-color: #666666;
		background-color: #dedede;
	}
	table.gridtable td {
		border-width: 1px;
		padding: 8px;
		border-style: solid;
		border-color: #666666;
		background-color: #ffffff;
	}
</style>

<body>
	<h2 align="center">通过Dubbo服务获取的数据列表</h2>
	<br>
	
	<table class="gridtable" align="center" width="450">
		<tr align="right">
			<td colspan="4"><a href="<%=ctx %>/toAdd.html">添加</a></td>
		</tr>
		<tr>
			<th>编号</th>
			<th>用户名</th>
			<th>邮箱</th>
			<th>操作</th>
		</tr>
		<c:forEach items="${userList}" var="user">
			<tr>
				<td>${user.id}</td>
				<td><a href="<%=ctx %>/showDetail.html?id=${user.id}">${user.userName}</a></td>
				<td>${user.email}</td>
				<td><a href="<%=ctx %>/del.html?id=${user.id}">删除</a></td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>