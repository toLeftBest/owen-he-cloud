<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户详细信息</title>
<%
	String ctx = request.getContextPath();
%>
</head>
<body>
	<table align="center">
		<tr>
			<td>ID</td>
			<td><input type="text" name="id" value="${user.id}" disabled="disabled"></td>
		</tr>
		
		<tr>
			<td>Name</td>
			<td><input type="text" name="userName" value="${user.userName}" disabled="disabled"></td>
		</tr>
		
		<tr>
			<td>Email</td>
			<td><input type="text" name="email" value="${user.email}" disabled="disabled"></td>
		</tr>
		
		
		<tr align="center">
			<th><input type="button" value="返回" onclick="location.href='<%=ctx%>/index.html'"></th>
		</tr>
	</table>
</body>
</html>