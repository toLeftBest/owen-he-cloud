package com.owen.he.jee.cloud.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.owen.he.jee.cloud.model.User;
import com.owen.he.jee.cloud.service.IUserService;

/**
 * 
 * <p>
 *
 * @author   WYS
 * @date	 2015年10月21日
 * @version  V1.0.0
 */
@Controller
@RequestMapping("")
public class IndexController extends BaseController {

	@Resource(name = "userService")
	protected IUserService userService;


	@RequestMapping("index")
	public String home( Model model ) {
		List<User> userList = userService.selectList();
		model.addAttribute("userList", userList);
		return "index/index";
	}


	@RequestMapping("toAdd")
	public String toAdd() {
		return "index/add";
	}


	@RequestMapping("add")
	public String add( User user ) {
		Long userId = userService.insertEntity(user);
		return "redirect:index.html";
	}


	@RequestMapping("showDetail")
	public String showDetail( @RequestParam("id" ) Long id, Model model) {
		User user = userService.selectObjectById(id);
		model.addAttribute("user", user);
		return "index/detail";
	}


	@RequestMapping("del")
	public String del( @RequestParam("id" ) Long id, Model model) {
		userService.del(id);
		return "redirect:index.html";
	}

}
