/**
 * 
 */
package com.owen.he.jee.cloud.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.dubbo.common.utils.ReflectUtils;
import com.alibaba.dubbo.rpc.Filter;
import com.alibaba.dubbo.rpc.Invocation;
import com.alibaba.dubbo.rpc.Invoker;
import com.alibaba.dubbo.rpc.Result;
import com.alibaba.dubbo.rpc.RpcException;
import com.alibaba.dubbo.rpc.RpcInvocation;

/**
 * @author chenly 
 *
 * @version createtime:2016-12-1 上午9:20:05 
 */
public class PasCloudLogFilter implements Filter {
	
	private static Logger log = LoggerFactory.getLogger(PasCloudLogFilter.class);

	/* (non-Javadoc)
	 * @see com.alibaba.dubbo.rpc.Filter#invoke(com.alibaba.dubbo.rpc.Invoker, com.alibaba.dubbo.rpc.Invocation)
	 */
	@Override
	public Result invoke(Invoker<?> invoker, Invocation invocation)
			throws RpcException {
		// TODO Auto-generated method stub
		log.info("filter log.......");
//		HttpServletRequest request = (HttpServletRequest)RpcContext.getContext().getRequest();  
//        HttpServletResponse response = (HttpServletResponse)RpcContext.getContext().getResponse(); 
//        log.info(request.toString());
//        log.info(g.toJson(request));
		
		Result result = invoker.invoke(invocation); 
		log.info(invoker.getUrl().toFullString());
		StringBuilder sn = new StringBuilder();
		Class<?>[] types = invocation.getParameterTypes();
        if (types != null && types.length > 0) {
            boolean first = true;
            for (Class<?> type : types) {
                if (first) {
                    first = false;
                } else {
                    sn.append(",");
                }
                
                sn.append(type.getName());
            }
        }
        log.info(sn.toString());
        
        String[] types0 = new String[types.length];
        for (int i = 0; i < types.length; i ++) {
            types0[i] = ReflectUtils.getName(types[i]);
            
        }
        RpcInvocation invocation2 = (RpcInvocation) invocation;
        
        
		return result;
	}

}
