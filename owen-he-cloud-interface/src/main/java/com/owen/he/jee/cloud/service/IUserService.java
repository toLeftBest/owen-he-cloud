package com.owen.he.jee.cloud.service;

import java.util.List;

import com.owen.he.jee.cloud.model.User;

public interface IUserService {

	/**
	 * 
	 * 测试输出传入的数据信息
	 * <p>
	 *
	 * @param str
	 * @return 
	 */
	public String printMsg( String str );


	/**
	 * 插入操作，插入实体数据
	 * @param user
	 * @return
	 */
	public Long insertEntity( User user );


	public User selectObjectById( Long id );


	public List<User> selectList();


	public void del( Long id );
}
