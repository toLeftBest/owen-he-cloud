package com.owen.he.jee.cloud.service;


public interface ITestService {

	/**
	 * 
	 * 测试输出传入的数据信息
	 * <p>
	 *
	 * @param str
	 * @return 
	 */
	public String printMsg( String str );
}
