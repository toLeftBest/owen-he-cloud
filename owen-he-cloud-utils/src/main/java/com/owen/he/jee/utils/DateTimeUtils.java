package com.owen.he.jee.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;


public class DateTimeUtils {

	/**
	 * 计算一定天数后的时间并取23点59分59秒，用于计算付款截至时间
	 * @param days 
	 */
	public static Timestamp getDayDelay(int days){
		Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 0);
        cal.add(Calendar.DAY_OF_MONTH, days);
        return  new Timestamp(cal.getTimeInMillis());
	}
	
	
	/**
	 * 当天是否到达截至时间
	 * @param day 截至日
	 */
	public static boolean isClosingDay(int closingDay, int delay){
		if(closingDay<1) return false;
		Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -delay-1);//第二天凌晨执行，所以减一天
        final int currentDay = cal.get(Calendar.DAY_OF_MONTH);
        final int lastDay = cal.getActualMaximum(Calendar.DATE);
        if(closingDay==currentDay){//到达截至时间
        	return true;
        }else if(lastDay<closingDay && currentDay==lastDay){ //如当月最后一天仍无法到达截至时间则返回真
        	return true;
        }
		return false;
	}
	
	
	/**
	 * 根据formatString将Date转成字符串
	 * 
	 * @param date
	 *            日期
	 * @param formatString
	 *            转换格式
	 * @return 字符串
	 */
	public static String date2String(Date date, String formatString) {
		if (date == null) {
			return null;
		}
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatString);
		return simpleDateFormat.format(date);
	}
	
	
	/**
	 * 日期Date增加指定天数
	 * 
	 * @param date
	 *            日期Date
	 * @param daynum
	 *            天数
	 * @return 增加天数后的Date
	 */
	public static Date addDate(final Date date, final int daynum) {
		int minute = daynum * 60 * 24;
		return addTime(date, minute);
	}
	
	/**
	 * 日期Date增加指定分钟数
	 * 
	 * @param startDate
	 *            日期Date
	 * @param minute
	 *            分钟数
	 * @return 增加分钟数后的Date
	 */
	public static Date addTime(final Date startDate, final int minute) {
		long millis = startDate.getTime();
		millis = millis + (minute * 60L * 1000L);
		Date date = new Date(millis);
		return date;
	}
	
	/**
	 * 获取上个月第一天
	 * 
	 * @param date
	 * @return
	 */
	public static Timestamp getPerMonthFirstDay() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -1);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		return new Timestamp(calendar.getTimeInMillis());
	}

	/**
	 * 获取上个月最后一天
	 * 
	 * @param date
	 * @return
	 */
	public static Timestamp getPerMonthLastDay() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.DAY_OF_MONTH, 1); 
		calendar.add(Calendar.DATE, -1);
		return new Timestamp(calendar.getTimeInMillis());
	}
	
	/**
	 * 获取一天开始时间
	 * 
	 * @param date
	 * @return
	 */
	public static Date getDayStart(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	/**
	 * 获取一天结束时间
	 * 
	 * @param date
	 * @return
	 */
	public static Date getDayEnd(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		return calendar.getTime();
	}
	
	public static Timestamp getPayClosingDate(int days, int minDays, int month){
		Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 0);
        if(month>0) {
        	cal.add(Calendar.MONTH, 1);
        }
        cal.set(Calendar.DAY_OF_MONTH, minDays);
        cal.add(Calendar.DAY_OF_MONTH, days);
        return  new Timestamp(cal.getTimeInMillis());
	}
	
	public static int getNowMonthOfDay() {
		Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
		int day = cal.get(Calendar.DAY_OF_MONTH);
		return day;
	}
	
	public static void main(String args[]){
		//System.out.println(isClosingDay(6,-1));
		//System.out.println(getNowMonthOfDay());
	}


	public static String changeDateToStr(Timestamp time) {
		if (time != null) {
			DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
			return format.format(time);
		} else {
			return "";
		}
	}
}
