package com.owen.he.jee.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.apache.commons.lang3.StringUtils;

public class UnitConvertUtils {
	
	/**
	 * 将分转换为元的字符串
	 * @param fen
	 * @return
	 */
	public static String fenToYuan(int fen) {
		return new BigDecimal(fen).divide(new BigDecimal(100), 2, RoundingMode.UNNECESSARY).toString();
	}
	
	/**
	 * 将字符串yuan转成分或者percentValue扩大100(用户新增数据)
	 * @param yuan
	 * @return
	 */
	public static int yuanToFenInt(String yuan) {
		if (StringUtils.isBlank(yuan)) {
			return 0;
		}
		return new BigDecimal(100).multiply(new BigDecimal(yuan)).intValue();
	}
	
	/**
	 * 将字符串yuan转成分或者percentValue扩大100(用户新增数据)
	 * @param yuan
	 * @return
	 */
	public static String yuanToFenStr(String yuan) {
		if (StringUtils.isBlank(yuan)) {
			return "0";
		}
 		return new BigDecimal(100).multiply(new BigDecimal(yuan)).intValue()+"";
	}
	
	/**
	 * 将字符串fen转成yuan或者percentValue缩小100(用户新增数据)
	 * @param yuan
	 * @return
	 */
	public static String fenToYuanStr(String yuan) {
		if (StringUtils.isBlank(yuan)) {
			return "0";
		}
		return  new BigDecimal(yuan).divide(new BigDecimal(100), 2, RoundingMode.UNNECESSARY).toString();
	}
	
	/**
	 * 将系统存储的百分比数值转换为百分比字符串
	 * @param percentValue
	 * @return
	 */
	public static String valueToPercent(int percentValue) {
		return new BigDecimal(percentValue).divide(new BigDecimal(100), 2, RoundingMode.UNNECESSARY).toString() + "%";
	}
	
	/**
	 * 将百分比字符串转换为系统可存储的百分比数值
	 * @param sPercent 百分比字符串"5.6%"或者"5.6"
	 * @return
	 * @author bobo
	 */
	public static int toPercent(String sPercent) {
		if (StringUtils.isBlank(sPercent)) {
			return 0;
		}
		return new BigDecimal(StringUtils.replace(sPercent, "%", "")).multiply(new BigDecimal(100)).intValue();
	}
	
	
	/**
	 * 目标值大于最大值取最大值，目标值小于最小值取最小值，中间不变
	 * @param min 最小值
	 * @param max 最大
	 * @param target 目标值
	 * @return
	 * @author owen.he
	 */
	public static int getBaseNumber(int min, int max, int target) {
		if (target < min) {
			return min;
		} else if (target > max) {
			return max;
		}
		return target;
	}
	
	public static String getPercentNumber(String percents,String targetPercent) {
		
		String[] percent = percents.split(",");
		boolean isExist = false;
		for (String thePercent : percent) {
			if(targetPercent.equals(thePercent)){
				isExist = true;
			}
		}
		if(isExist){
			return targetPercent;
		}else{
			int targetPercentNumber = Integer.parseInt(targetPercent);
			int maxPercent = Integer.parseInt(percent[0]);
			int minPercent = Integer.parseInt(percent[0]);
			for (int i=0;i<percent.length;i++) {
				int currentPercent = Integer.parseInt(percent[i]);
				if (currentPercent > maxPercent)// 判断最大值
					maxPercent = currentPercent;
				if (currentPercent < minPercent)// 判断最小值
					minPercent = currentPercent;
			}
			float halfPercent = (float) ((maxPercent+minPercent)*1.0/2);
			if(halfPercent>targetPercentNumber){
				return StringUtils.join(minPercent);
			}else {
				return StringUtils.join(maxPercent);
			}
		}
	}
	
	public static void main(String[] args) {
		String s = "50,60,159,250";
		System.out.println(getPercentNumber(s, 20+""));
	}
	
}
