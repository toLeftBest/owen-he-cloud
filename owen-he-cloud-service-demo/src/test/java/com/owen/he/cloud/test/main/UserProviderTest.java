package com.owen.he.cloud.test.main;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UserProviderTest {

	public static void main( String[] args ) throws Exception {
		String[] springConfigFiles = { "spring/applicationContext-dataSource.xml", "spring/applicationContext.xml",
				"spring/applicationContext-dubbo-provider.xml" };

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(springConfigFiles);

		context.start();

		System.in.read(); // 按任意键退出
	}


}
