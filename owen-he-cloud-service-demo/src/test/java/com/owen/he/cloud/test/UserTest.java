package com.owen.he.cloud.test;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.owen.he.jee.cloud.model.User;
import com.owen.he.jee.cloud.service.IUserService;

import junit.framework.TestCase;

/**
 * 接口测试
 * <p>
 *
 * @author   WYS
 * @date	 2015年10月19日
 * @version  V1.0.0
 */
@RunWith(OrderedRunner.class)
public class UserTest extends TestCase {

	private IUserService userService;

	private ApplicationContext ctx = null;


	@Before
	@Order(order = 1)
	public void prepare() throws Exception {
		//可以加载多个配置文件(注：多个文件需要以逗号隔开)
		String[] springConfigFiles = { "spring/applicationContext-dataSource.xml", "spring/applicationContext.xml",
				"spring/applicationContext-dubbo-provider.xml" };

		ctx = new ClassPathXmlApplicationContext(springConfigFiles);
		userService = (IUserService) ctx.getBean("userService");
	}


	@Test
	@Order(order = 2)
	public void printData0() {
		String msg = userService.printMsg("张无忌");
		System.out.println(msg);
	}


	@Test
	@Order(order = 3)
	public void insertEntityTest() {
		User user = new User();
		user.setId(1001L);
		user.setUserName("星野千里");
		Long userId = userService.insertEntity(user);
		System.out.println("current user id is :" + userId);
	}
}
