package com.owen.he.cloud.test;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 
 * 
 * <p>
 *
 * @author   WYS
 * @date	 2015年10月19日
 * @version  V1.0.0
 */
@Retention(RetentionPolicy.RUNTIME)
public abstract @interface Order {

	public int order();
}
