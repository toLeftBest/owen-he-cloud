package com.owen.he.jee.cloud.mapper;

import java.util.List;

import com.owen.he.jee.cloud.model.User;

/**
 * 
 * <p>
 *
 * @author   WYS
 * @date	 2015年10月20日
 * @version  V1.0.0
 */
public interface UserMapper {

	public Long insertEntity( User user );


	public User selectObjectById( Long id );


	public List<User> selectList();


	public void del( Long id );
}
