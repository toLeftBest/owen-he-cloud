package com.owen.he.jee.cloud.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.owen.he.jee.cloud.mapper.UserMapper;
import com.owen.he.jee.cloud.model.User;
import com.owen.he.jee.cloud.service.IUserService;


/**
 * 用户接口
 * <p>
 *
 * @author   WYS
 * @date	 2015年10月19日
 * @version  V1.0.0
 */
public class UserServiceImpl implements IUserService {

	@Resource(name = "userMapper")
	private UserMapper userMapper;


	@Override
	public String printMsg( String str ) {
		return "您好，" + str;
	}


	/**
	 * 此方法返回主键id
	 * <p>
	 * 如果insert操作更新的行数  >0 的话，返回主键id; 如果更新的行数不大于0的话，返回0；
	 *
	 * @param user
	 * @return
	*/
	@Override
	public Long insertEntity( User user ) {
		Long insertEntityReturnId = userMapper.insertEntity(user);
		if ( insertEntityReturnId > 0 ) {
			return user.getId();
		} else {
			return 0L;
		}
	}


	/**
	 * <p>
	 *
	 * @param id
	 * @return 
	*/
	@Override
	public User selectObjectById( Long id ) {
		return userMapper.selectObjectById(id);
	}


	/**
	 * <p>
	 *
	 * @return
	*/
	@Override
	public List<User> selectList() {
		return userMapper.selectList();
	}


	@Override
	public void del( Long id ) {
		userMapper.del(id);
	}
}
