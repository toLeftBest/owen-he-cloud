/**
 * 
 */
package com.owen.he.jee.dao;

import com.owen.he.jee.cloud.model.User;


public interface UserMapperDao {


	/**
	 * @Description 判断代理商名称是否存在
	 * @param agentName
	 * @return
	 * @author chenyong
	 */
	User selectByPrimaryKey(Integer id);


}
