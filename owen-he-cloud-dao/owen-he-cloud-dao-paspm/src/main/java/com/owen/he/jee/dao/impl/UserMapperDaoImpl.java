/**
 * 
 */
package com.owen.he.jee.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.owen.he.jee.cloud.model.User;
import com.owen.he.jee.dao.UserMapperDao;
import com.owen.he.jee.dao.mybatis.UserMapper;

@Service
public class UserMapperDaoImpl implements UserMapperDao{

	@Autowired
	UserMapper userMapper;
	
	public UserMapper getUserMapper() {
		return userMapper;
	}

	public void setUserMapper(UserMapper userMapper) {
		this.userMapper = userMapper;
	}

	public List getList() {
		Map<String,Object> map = new HashMap<>();
		return userMapper.getAll();
	}

	@Override
	public User selectByPrimaryKey(Integer id) {
		return userMapper.selectByPrimaryKey(id);
	}
	
	
}
