package com.owen.he.jee.cloud.model;


/**
 * 用户实体对象
 * <p>
 *
 * @author   WYS
 * @date	 2015年10月19日
 * @version  V1.0.0
 */
public class User implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	/*
	 * 主键
	 */
	private Long id;

	private String userName;

	private String password;

	private Integer age;
	
	private String email;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName == null ? null : userName.trim();
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password == null ? null : password.trim();
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
